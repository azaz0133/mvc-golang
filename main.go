package main

import (
	"github.com/kataras/iris"
	"github.com/mvc/src/modules/config"
)

func main() {
	app := iris.New()

	app.Logger().SetLevel("debug")

	views := iris.HTML("./web/views", ".html").Layout("layout.html").Reload(true)

	app.RegisterView(views)

	app.StaticWeb("/public", "./web/public")

	app.Get("/", func(ctx iris.Context) {
		ctx.ViewData("Message", "Golang TEST")
		ctx.View("index.html")
	})

	db, err := config.GetMongoDB()

	app.Run(iris.Addr(":3000"))

}
