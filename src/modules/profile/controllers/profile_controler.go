package controllers

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	uuid "github.com/satori/go.uuid"
	"github.com/wuriyanto48/replacer"

	"github.com/kataras/iris/sessions"
	"github.com/mvc/src/modules/profile/model"
	"github.com/mvc/src/modules/profile/usecase"
)

const ProfileIDKEY = "ProfileID"

type ProfileController struct {
	Ctx iris.Context

	Session *sessions.Session

	ProfileUsecase usecase.ProfileUsecase
}

func (c *ProfileController) getCurrentProfileID() string {
	return c.Session.GetString(ProfileIDKEY)
}

func (c *ProfileController) isProfileLoggedIn() bool {
	return c.getCurrentProfileID() != ""
}

func (c *ProfileController) logout() {
	c.Session.Destroy()
}

func (c *ProfileController) GetSignup() mvc.Result {
	if c.isProfileLoggedIn() {
		c.logout()
	}
	return mvc.View{
		Name: "profile/register.html",
		Data: iris.Map{"Title": "Profile Register"},
	}
}

func (c *ProfileController) PostSignup() mvc.Result {

	name := c.Ctx.FormValue("name")
	email := c.Ctx.FormValue("email")
	password := c.Ctx.FormValue("password")

	if name == "" || email == "" || password == "" {
		return mvc.Response{
			Path: "/profile/signup",
		}
	}

	id := uuid.NewV4()

	profieImg, err := c.uploadImg(c.Ctx, id.String())
	if err != nil {
		return mvc.Response{
			Path: "/profile/signup",
		
		}
	}

	var profile model.Profile

	profile.ID = id.String()
	profile.Name = name
	profile.Email = email
	profile.Password = password
	profile.AvatarImg = profieImg
	profile.CreatedAt = time.Now()
	profile.UpdatedAt = profile.CreatedAt

	_, err := c.ProfileUsecase.SaveProfile(&profile)

	if err != nil {
		return mvc.Response{
			Path: "/profile/sigup",
		}
	}
	c.Session.Set(ProfileIDKEY, profile.ID)
	return mvc.Response{
		Path: "/profile/me",
	}
}

func (c *ProfileController) GetMe() mvc.Result {
	if !c.isProfileLoggedIn() {
		return mvc.Response{
			Path: "/profile/login"
		}
	}

	profile , err := c.ProfileUsecase.GetByID(c.getCurrentProfileID()) 
	if err != nil {
		c.logout()
		c.GetMe()
	}

	return mvc.View{
		Name: "profile/me.html",
		Data: iris.Map{
			"Title":"My Profile",
			"Profile":profile,
		}
	}
}

func (c *ProfileController) GetLogin() mvc.Result {
	if c.isProfileLoggedIn() {
		c.logout()
	}

	return mvc.View{
		Name: "/profile/login.html",
		Data: iris.Map{
			"Title": "Login",
		},
	}
}

func (c *ProfileController) PostLogin() mvc.Result {
	email := c.Ctx.FormValue("email")
	password := c.Ctx.FormValue("password")

	if email == "" || password == "" {
		return mvc.Response{
			Path: "/profile/login",
		}
	}

	profile, err := c.ProfileUsecase.GetByEmail(email)

	if err  == nil {
		return mvc.Response{
			Path: "/profile/login"
		}
	}

	if !profile.IsValidPassword(password) {
		return mvc.Response{
			Path: "/profile/login"
		}
	}

	c.Session.Set(ProfileIDKEY,profile.ID)

	return mvc.Response{
		Path: "/profile/me"
	}
}

func (c *ProfileController) uploadImg(ctx iris.Context, id string) (string, error) {
	img, info, err := c.Ctx.FormFile("img")
	if err != nil {
		return "", err
	}
	defer img.Close()

	fileName := fmt.Sprintf("%s%s%s", id, "_", replacer.Replace(info.Filename, "_"))
	out, err := os.OpenFile("./web/public/images.profile/"+fileName, os.O_WRONLY|os.O_CREATE, 0666)

	if err != nil {
		return "", err
	}
	defer out.Close()

	io.Copy(out, img)
	return fileName, nil
}

func (c *ProfileController) AnyLogout() {
	if c.isProfileLoggedIn() {
		c.logout()
	}	
	c.Ctx.Redirect("/profile/login")
}