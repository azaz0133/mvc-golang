package model

import (
	"time"
)

type Profile struct {
	ID        string    `bson:"id"`
	Name      string    `bson:"name"`
	Email     string    `bson:"email"`
	Password  string    `bson:"password"`
	AvatarImg string    `bson:"img"`
	CreatedAt time.Time `bson:"created_at"`
	UpdatedAt time.Time `bson:"updated_at"`
}

type Profiles []Profile

func (p *Profile) IsValidPassword(password string) bool {
	return p.Password == password
}
